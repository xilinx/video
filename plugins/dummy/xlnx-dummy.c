/*
 * Xilinx Video Library - Dummy plugin that supports no device
 *
 * Copyright (C) 2016 Ideas on board Oy
 *
 * Contact: Laurent Pinchart <laurent.pinchart@ideasonboard.com>
 *
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at
 * your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 */

#include <stdlib.h>

#include <xlnx-plugin.h>

static const struct xlnx_video_plugin_info dummy_plugin_info = {
	.name = "Dummy plugin",
	.type = XLNX_VIDEO_PLUGIN_TYPE_SOFTWARE,
};

static const struct xlnx_video_plugin_info *dummy_info(void)
{
	return &dummy_plugin_info;
}

static struct xlnx_video_component *dummy_scan(struct xlnx_video *xv,
					       struct media_device *mdev)
{
	return NULL;
}

EXPORT_XLNX_VIDEO_PLUGIN(dummy) = {
	.info = dummy_info,
	.scan = dummy_scan,
};
