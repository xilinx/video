/*
 * Xilinx Video Library - Buffers Pool
 *
 * Copyright (C) 2014-2016 Ideas on board Oy
 *
 * Contact: Laurent Pinchart <laurent.pinchart@ideasonboard.com>
 *
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at
 * your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 */
#ifndef __XLNX_VIDEO_BUFFERS_POOL_H__
#define __XLNX_VIDEO_BUFFERS_POOL_H__

#include <stdbool.h>
#include <time.h>

/*
 * struct xlnx_video_buffer - Video buffer information
 * @index: Zero-based buffer index, limited to the number of buffers minus one
 * @size: Size of the video memory, in bytes
 * @bytesused: Number of bytes used by video data, smaller or equal to @size
 * @timestamp: Time stamp at which the buffer has been captured
 * @error: True if an error occured while capturing video data for the buffer
 * @allocated: True if memory for the buffer has been allocated by the pool
 * @mem: Video data memory
 */
struct xlnx_video_buffer
{
	unsigned int index;
	unsigned int size;
	unsigned int bytesused;
	struct timeval timestamp;
	bool error;
	bool allocated;
	void *mem;
};

struct xlnx_video_buffers_pool
{
	unsigned int nbufs;
	struct xlnx_video_buffer *buffers;
};

/*
 * xlnx_video_buffers_pool_new - Create a new buffers pool
 * @nbufs: Number of buffers in the pool
 *
 * Allocate a new buffers pool with space for @nbufs buffers. Memory for the
 * buffers is not allocated.
 */
struct xlnx_video_buffers_pool *xlnx_video_buffers_pool_new(unsigned int nbufs);

/*
 * xlnx_video_buffers_pool_delete - Delete a buffers pool
 * @pool: Buffers pool
 *
 * Delete a buffers pool and free buffers memory if it has been allocated by
 * the pool.
 */
void xlnx_video_buffers_pool_delete(struct xlnx_video_buffers_pool *pool);

/*
 * xlnx_video_buffers_pool_alloc - Allocate memory for buffers in a pool
 * @pool: Buffers pool
 * @size: Buffer size
 * @align: Buffer memory alignment in bytes
 *
 * Allocate @size bytes of memory for each buffer aligned to a multiple of
 * @align bytes. The alignment must be a power of 2.
 *
 * Return 0 on success or a negative error code on failure.
 */
int xlnx_video_buffers_pool_alloc(struct xlnx_video_buffers_pool *pool,
				  size_t size, size_t align);

#endif /* __XLNX_VIDEO_BUFFERS_POOL_H__ */
