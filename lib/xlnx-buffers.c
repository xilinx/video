/*
 * Xilinx Video Library - Buffers Pool
 *
 * Copyright (C) 2014-2016 Ideas on board Oy
 *
 * Contact: Laurent Pinchart <laurent.pinchart@ideasonboard.com>
 *
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at
 * your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 */

#include <stdlib.h>
#include <string.h>

#include "xlnx-buffers.h"

struct xlnx_video_buffers_pool *xlnx_video_buffers_pool_new(unsigned int nbufs)
{
	struct xlnx_video_buffer *buffers;
	struct xlnx_video_buffers_pool *pool;
	unsigned int i;

	pool = malloc(sizeof *pool);
	if (pool == NULL)
		return NULL;

	buffers = malloc(sizeof *buffers * nbufs);
	if (buffers == NULL) {
		free(pool);
		return NULL;
	}

	memset(buffers, 0, sizeof *buffers * nbufs);

	for (i = 0; i < nbufs; ++i)
		buffers[i].index = i;

	pool->nbufs = nbufs;
	pool->buffers = buffers;

	return pool;
}

void xlnx_video_buffers_pool_delete(struct xlnx_video_buffers_pool *pool)
{
	unsigned int i;

	for (i = 0; i < pool->nbufs; ++i) {
		if (pool->buffers[i].allocated)
			free(pool->buffers[i].mem);
	}

	free(pool->buffers);
	free(pool);
}

int xlnx_video_buffers_pool_alloc(struct xlnx_video_buffers_pool *pool,
				  size_t size, size_t align)
{
	unsigned int i;
	int ret;

	for (i = 0; i < pool->nbufs; ++i) {
		ret = posix_memalign(&pool->buffers[i].mem, align, size);
		if (ret != 0)
			return -ret;

		pool->buffers[i].size = size;
		pool->buffers[i].allocated = true;
	}

	return 0;
}
