/*
 * Xilinx Video Library - Internal API
 *
 * Copyright (C) 2014-2016 Ideas on board Oy
 *
 * Contact: Laurent Pinchart <laurent.pinchart@ideasonboard.com>
 *
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at
 * your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 */
#ifndef __XLNX_PRIV_H__
#define __XLNX_PRIV_H__
/** \cond INTERNAL_API */

#include <stdbool.h>
#include <stdlib.h>

#include <xlnx-list.h>
#include <xlnx-video.h>

struct media_device;
struct media_entity;
struct media_entity_desc;

/**
 * \brief Xilinx video entity in a component
 */
struct xlnx_video_entity {
	/** Entities list */
	struct list_entry list;
	/** Media entity */
	struct media_entity *entity;
	/** Media entity information */
	const struct media_entity_desc *info;
	/** Indicates whether this is the last entity in the component */
	bool last;
};

/**
 * \brief Xilinx video component
 */
struct xlnx_video_component {
	/** The library context */
	struct xlnx_video *xv;
	/** List entry in the context's components list */
	struct list_entry list;
	/** List of video entities in the component */
	struct list_entry entities;

	/** Component type */
	enum xlnx_video_component_type type;
	/** Component name */
	const char *name;

	/**
	 * Entity at the input of the component (NULL if the component has no
	 * input entity).
	 */
	struct xlnx_video_entity *input;
	/**
	 * Entity at the output of the component (NULL if the component has no
	 * output entity).
	 */
	struct xlnx_video_entity *output;
};

/**
 * \brief Xilinx video library context
 */
struct xlnx_video {
	/** List of video components in the system */
	struct list_entry components;
	/** Number of components in the components list */
	unsigned int num_components;

	/** List of loaded plugins */
	struct list_entry plugins;

	/** Event handler operations */
	const struct xlnx_video_event_operations *event_ops;
	/** Event handler private data */
	void *event_handler;

	/** Log level for library messages */
	enum xlnx_log_level log_level;
};

int xlnx_video_plugin_init(struct xlnx_video *xv);
void xlnx_video_plugin_fini(struct xlnx_video *xv);
struct xlnx_video_component *xlnx_video_plugin_scan(struct xlnx_video *xv,
						    struct media_device *mdev);

void xlnx_video_log_print(struct xlnx_video *xv, unsigned int level,
			  const char *func, const char *fmt, ...);

#define xlnx_dbg(xv, fmt...) \
	xlnx_video_log_print(xv, XLNX_LOG_DEBUG, __func__, fmt)

/** \endcond INTERNAL_API */
#endif /* __XLNX_PRIV_H__ */
