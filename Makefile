SUBDIRS := 3rdparty doc lib plugins src

export topdir := $(PWD)

all: all-recursive

clean: clean-recursive

doc:
	$(MAKE) -C doc $@

install: install-recursive

.PHONY: doc

recursive=all-recursive clean-recursive install-recursive

$(recursive):
	@target=`echo $@ | sed s/-recursive//` ; \
	for subdir in $(SUBDIRS); do \
		echo "Making $$target in $$subdir" ; \
		$(MAKE) -C $$subdir $$target; \
	done
