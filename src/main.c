/*
 * Xilinx Video Library - Command line sample application
 *
 * Copyright (C) 2016 Ideas on board Oy
 *
 * Contact: Laurent Pinchart <laurent.pinchart@ideasonboard.com>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * 3. The name of the author may not be used to endorse or promote products
 * derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY [LICENSOR] "AS IS" AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO
 * EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <getopt.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>

#include <xlnx-events.h>
#include <xlnx-tools.h>
#include <xlnx-video.h>

static const char *component_type_name(enum xlnx_video_component_type type)
{
	static const struct {
		enum xlnx_video_component_type type;
		const char *name;
	} types[] = {
		{ XLNX_VIDEO_COMPONENT_CAPTURE, "capture" },
		{ XLNX_VIDEO_COMPONENT_PROCESS, "process" },
		{ XLNX_VIDEO_COMPONENT_DISPLAY, "display" },
	};
	unsigned int i;

	for (i = 0; i < ARRAY_SIZE(types); ++i) {
		if (types[i].type == type)
			return types[i].name;
	}

	return "unknown";
}

static void list_components(struct xlnx_video *xv)
{
	struct xlnx_video_component **components;
	int num_components;
	int i;

	num_components = xlnx_video_component_list(xv, XLNX_VIDEO_COMPONENT_ANY,
						   &components);
	if (num_components < 0) {
		printf("Failed to list components\n");
		return;
	} else if (!num_components) {
		printf("No components found.\n");
		return;
	}

	printf("%u component%s found\n", num_components,
	       num_components > 1 ? "s" : "");

	for (i = 0; i < num_components; ++i) {
		const struct xlnx_video_component *component = components[i];
		enum xlnx_video_component_type type;
		const char *name;

		type = xlnx_video_component_type(component);
		name = xlnx_video_component_name(component);

		printf("- Component %u: type %s name %s\n", i,
		       component_type_name(type), name);
	}

	free(components);
}

/* -----------------------------------------------------------------------------
 * Command Line Parsing
 */

static unsigned int opt_debug;
static bool opt_list_components;
static const char *opt_plugins_directory;

static void usage(const char *argv0)
{
	printf("%s [options]\n", argv0);
	printf("-c, --components	List available components\n");
	printf("-d, --debug level	Set the debug level (0-4, default: 0)\n");
	printf("-h, --help		Show this help message and exit\n");
	printf("-p, --plugins dir	Load plugins from given directory\n");
}

static struct option opts[] = {
	{"components", 0, 0, 'c'},
	{"debug", 1, 0, 'd'},
	{"help", 0, 0, 'h'},
	{"plugins", 1, 0, 'p'},
};

static int parse_cmdline(int argc, char **argv)
{
	char *endp;
	int opt;

	/* Parse options */
	while ((opt = getopt_long(argc, argv, "cd:hp:", opts, NULL)) != -1) {
		switch (opt) {
		case 'c':
			opt_list_components = true;
			break;

		case 'd':
			opt_debug = strtoul(optarg, &endp, 10);
			if (*endp != '\0') {
				fprintf(stderr, "Invalid debug level %s\n", optarg);
				usage(argv[0]);
				return 1;
			}
			break;

		case 'h':
			usage(argv[0]);
			return 1;

		case 'p':
			opt_plugins_directory = optarg;
			break;
		}
	}

	return 0;
}

/* -----------------------------------------------------------------------------
 * Main
 */

int main(int argc, char *argv[])
{
	struct xlnx_video *xv;
	int ret;

	ret = parse_cmdline(argc, argv);
	if (ret)
		return ret;

	xv = xlnx_video_create();
	if (!xv) {
		printf("Failed to create a library context\n");
		return 1;
	}

	if (opt_debug)
		xlnx_video_set_log_level(xv, opt_debug);

	ret = xlnx_events_init(xv);
	if (ret < 0) {
		printf("Failed to initialize events handling\n");
		goto done;
	}

	ret = xlnx_video_setup(xv);
	if (ret < 0) {
		printf("Failed to setup the library context\n");
		goto done;
	}

	if (opt_plugins_directory) {
		ret = xlnx_video_plugin_load_directory(xv, opt_plugins_directory);
		if (ret < 0) {
			printf("Failed to load plugins from directory `%s'\n",
			       opt_plugins_directory);
			goto done;
		}

		printf("Loaded %u plugin%s from directory `%s'\n", ret,
		       ret != 1 ? "s" : "", opt_plugins_directory);
	}

	ret = xlnx_video_component_enumerate(xv);
	if (ret < 0) {
		printf("Failed to enumerate components\n");
		goto done;
	}

	if (opt_list_components)
		list_components(xv);

done:
	xlnx_events_fini(xv);
	xlnx_video_destroy(xv);
	return ret;
}
