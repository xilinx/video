/*
 * Xilinx Video Library - Plugin API
 *
 * Copyright (C) 2014-2016 Ideas on board Oy
 *
 * Contact: Laurent Pinchart <laurent.pinchart@ideasonboard.com>
 *
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at
 * your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 */

#ifndef __XLNX_PLUGIN_H__
#define __XLNX_PLUGIN_H__

/**
 * \file
 * \brief Library plugin support
 *
 * To be discovered by the Xilinx video library, plugins must export a plugin
 * operations structure with a well-defined symbol name. Plugins should use the
 * EXPORT_XLNX_VIDEO_PLUGIN() macro to declare the strucure and initialize its
 * members with function pointers.
 *
 * \code{.c}
 * EXPORT_XLNX_VIDEO_PLUGIN(myplugin) = {
 *	.info = myplugin_info,
 *	.scan = myplugin_scan,
 * }
 * \endcode
 */

#include <xlnx-video.h>

struct media_device;
struct xlnx_video_compoment;

/**
 * \brief Type of video component supported by the plugin
 */
enum xlnx_video_plugin_type {
	/**
	 * Hardware video component, handled automatically when the plugin is
	 * loaded or the component discovered.
	 */
	XLNX_VIDEO_PLUGIN_TYPE_HARDWARE,
	/**
	 * Software video component, instantiated manually.
	 */
	XLNX_VIDEO_PLUGIN_TYPE_SOFTWARE,
};

/**
 * \brief Plugin information
 */
struct xlnx_video_plugin_info {
	/** Plugin name */
	const char *name;
	/** Plugin type, one of XLNX_VIDEO_PLUGIN_TYPE_* */
	enum xlnx_video_plugin_type type;
};

/**
 * \brief Xilinx video plugin operations
 */
struct xlnx_video_plugin_ops {
	/**
	 * Query plugin information. The returned structure must be valid for
	 * the lifetime of the plugin.
	 */
	const struct xlnx_video_plugin_info *(*info)(void);
	struct xlnx_video_component *(*scan)(struct xlnx_video *xv,
					     struct media_device *mdev);
	struct xlnx_video_component *(*create_component)(struct xlnx_video *xv);
	void (*destroy_component)(struct xlnx_video *xv,
				  struct xlnx_video_compoment *xvcomp);
	/** Reserved fields for ABI compatibility, must be set to NULL. */
	void (*reserved[10])(void);
};

/** \cond IGNORE */
#define XLNX_VIDEO_PLUGIN_SYMBOL	__xlnx_video_plugin
#define EXPORT_XLNX_VIDEO_PLUGIN(name)					\
const struct xlnx_video_plugin_ops XLNX_VIDEO_PLUGIN_SYMBOL
/** \endcond */

#endif /* __XLNX_PLUGIN_H__ */
