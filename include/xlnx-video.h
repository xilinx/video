/*
 * Xilinx Video Library
 *
 * Copyright (C) 2014-2016 Ideas on board Oy
 *
 * Contact: Laurent Pinchart <laurent.pinchart@ideasonboard.com>
 *
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at
 * your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 */

#ifndef __XLNX_VIDEO_H__
#define __XLNX_VIDEO_H__

/**
 * \file
 * \brief Xilinx Video Library
 */

struct xlnx_video;
struct xlnx_video_component;
struct xlnx_video_pipeline;

struct xlnx_video *xlnx_video_create(void);
int xlnx_video_setup(struct xlnx_video *xv);
void xlnx_video_destroy(struct xlnx_video *xv);

/**
 * \brief Library log level
 *
 * Only messages with a log level lower than or equal to the library context
 * log level are logged, all other messages are dropped silently.
 *
 * \sa xlnx_log_set_level()
 */
enum xlnx_log_level {
	/** Don't log any message */
	XLNX_LOG_NONE = 0,
	/** Log error messages */
	XLNX_LOG_ERROR = 1,
	/** Log warning messages */
	XLNX_LOG_WARNING = 2,
	/** Log informational messages */
	XLNX_LOG_INFO = 3,
	/** Log debug messages */
	XLNX_LOG_DEBUG = 4,
};

void xlnx_video_set_log_level(struct xlnx_video *xv, enum xlnx_log_level level);

/**
 * \brief Event handler operations
 *
 * The library requires a set of event handling operations whose implementation
 * can vary between applications depending on their architecture. To avoid
 * restricting applications to a particular programming model, those operations
 * are not implemented by the library directly but must be provided by
 * applications in the form of callback functions.
 *
 * The xlnx_video_event_operations structure contains callback function
 * pointers for all event handling operations. Applications must supply an
 * implementation for each operation.
 *
 * All event handling operations are called by the library with the handler
 * parameter set to the event handler passed to the
 * xlnx_video_set_event_handler() function.
 */
struct xlnx_video_event_operations {
	/**
	 * \param handler event handler private data
	 * \param fd the file descriptor number
	 * \param events the types of events to watch for
	 * \param callback callback event handler
	 * \param priv private token passed to the callback event handler
	 *
	 * Start watching the file descriptor \a fd for events of type
	 * \a events, and call the \a callback function with the priv parameter
	 * set to \a priv when such an event is detected.
	 */
	void (*watch)(void *handler, int fd, int events,
		      void(*callback)(void *priv), void *priv);
	/**
	 * \param handler event handler private data
	 * \param fd the file descriptor number
	 *
	 * Stop watching the file descriptor \a fd for events.
	 */
	void (*unwatch)(void *handler, int fd);
};

void xlnx_video_set_event_handler(struct xlnx_video *xv,
				  const struct xlnx_video_event_operations *ops,
				  void *handler);
void *xlnx_video_get_event_handler_data(struct xlnx_video *xv);

int xlnx_video_plugin_load_directory(struct xlnx_video *xv, const char *path);

/**
 * \brief Video component type
 */
enum xlnx_video_component_type {
	/** Video capture component (input) */
	XLNX_VIDEO_COMPONENT_CAPTURE = 1 << 0,
	/** Video processing component (memory to memory) */
	XLNX_VIDEO_COMPONENT_PROCESS = 1 << 1,
	/** Video display component (output) */
	XLNX_VIDEO_COMPONENT_DISPLAY = 1 << 2,
	/** Any component type */
	XLNX_VIDEO_COMPONENT_ANY = XLNX_VIDEO_COMPONENT_CAPTURE
				 | XLNX_VIDEO_COMPONENT_PROCESS
				 | XLNX_VIDEO_COMPONENT_DISPLAY,
};

int xlnx_video_component_enumerate(struct xlnx_video *xv);
int xlnx_video_component_list(struct xlnx_video *xv,
			      enum xlnx_video_component_type type,
			      struct xlnx_video_component ***components);
const char *xlnx_video_component_name(const struct xlnx_video_component *xvcomp);
enum xlnx_video_component_type
xlnx_video_component_type(const struct xlnx_video_component *xvcomp);

#endif
