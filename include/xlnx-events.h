/*
 * Xilinx Video Library - select()-based generic event loop
 *
 * Copyright (C) 2014-2016 Ideas on board Oy
 *
 * Contact: Laurent Pinchart <laurent.pinchart@ideasonboard.com>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#ifndef __XLNX_EVENTS_H__
#define __XLNX_EVENTS_H__

/**
 * \file
 * \brief select()-based event loop implementation
 *
 * This event loop is a sample event handling operations implementation. It
 * registers event handler operations with the library context and implements an
 * infinite event loop based on the select() function to wait for events on
 * file descriptors.
 */

struct xlnx_video;

int xlnx_events_init(struct xlnx_video *xv);
void xlnx_events_fini(struct xlnx_video *xv);
bool xlnx_events_loop(struct xlnx_video *xv);
void xlnx_events_stop(struct xlnx_video *xv);

#endif /* __XLNX_EVENTS_H__ */
